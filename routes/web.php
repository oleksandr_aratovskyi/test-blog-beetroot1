<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('posts.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'posts', 'middleware' => ['auth']], function (){
    Route::get('', 'PostController@index')->name('posts.index');
    Route::get('create', 'PostController@create')->name('posts.create');
    Route::post('create', 'PostController@store');
    Route::group(['prefix' => '{id}'], function(){
        Route::get('', 'PostController@show')->name('posts.show');
        Route::get('edit', 'PostController@edit')->name('posts.edit');
        Route::post('edit', 'PostController@update');
        Route::get('delete', 'PostController@delete')->name('posts.delete');
    });
});