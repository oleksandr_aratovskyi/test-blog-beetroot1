## Blog

This is the small test blog implemented by next requirements:
- Create small blog
- Attach to Bitbucket for code review
- Blog fields (only 4)
  - title
  - body
  - photo
  - date posted
- Display, add, edit, delete functions