<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $image_link
 * @property int $user_id
 *
 * @method static static findOrFail($id)
 * @method static static create($params)
 *
 * @package App
 */
class Post extends Model
{
    protected $fillable = [
        'title',
        'body',
        'user_id'
    ];

    const IMG_NOT_AVAILABLE_URL = '/img/img_not_available.png';

    public function delete()
    {
        $deleted = parent::delete();
        if (!$deleted) {
            return false;
        }

        return $this->unlinkImage();
    }

    /**
     * Unlink the file connected with post image
     *
     * @return bool
     */
    protected function unlinkImage()
    {
        if (!$this->image_link) {
            return true;
        }
        if ($this->image_link == self::IMG_NOT_AVAILABLE_URL) {
            return true;
        }
        $fullPath = base_path() . '/public' . $this->image_link;
        return unlink($fullPath);
    }

    /**
     * Set default image for post if user hasn't specified one
     */
    public function setDefaultImage()
    {
        $this->unlinkImage();
        $image_link = self::IMG_NOT_AVAILABLE_URL;
        $this->image_link = $image_link;
        $this->save();
    }

    /**
     * Attach uploaded image to post
     *
     * @param UploadedFile $image
     *
     * @return void
     */
    public function attachImage($image)
    {
        $this->unlinkImage();
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $publicPath = 'storage/' . $this->id;
        $folder = public_path($publicPath);
        $image->move($folder, $filename);
        $image_link = '/' . $publicPath . '/' . $filename;
        $this->image_link = $image_link;
        $this->save();
    }
}
