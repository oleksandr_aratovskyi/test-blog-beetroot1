<?php

namespace App\Http\Controllers;

use App\Http\Requests\Posts\CreatePostRequest;
use App\Post;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * Class containing logic of actions with posts
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * Show the list of posts
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::query()
            ->where('user_id', Auth::id())
            ->orderBy('id', 'desc')
            ->paginate(5);

        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the post with specific ID
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for creating posts
     *
     * @return Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store the post in database
     *
     * @param CreatePostRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePostRequest $request)
    {
        $post = Post::create($request->toArray());
        if ($request->hasFile('photo')) {
            $post->attachImage($request->file('photo'));
        } else {
            $post->setDefaultImage();
        }

        return redirect()->route('posts.index');
    }

    /**
     * Show edit post form
     *
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        if ($post->user_id != Auth::id()) {
            throw new AuthorizationException();
        }
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update specified post in database
     *
     * @param CreatePostRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws AuthorizationException
     */
    public function update(CreatePostRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        if ($post->user_id != Auth::id()) {
            throw new AuthorizationException();
        }
        $post->update($request->toArray());
        if ($request->hasFile('photo')) {
            $post->attachImage($request->file('photo'));
        }

        return redirect()->route('posts.index');
    }

    /**
     * Delete the post from database
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws AuthorizationException
     */
    public function delete($id)
    {
        $post = Post::findOrFail($id);
        if ($post->user_id != Auth::id()) {
            throw new AuthorizationException();
        }
        $post->delete();
        return redirect()->route('posts.index');
    }
}
