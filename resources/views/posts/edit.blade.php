@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Edit the post</h1>
            </div>
            <div class="panel-body">
                <form method="POST" action="{{ route('posts.edit', $post->id) }}" enctype="multipart/form-data">
                    @include('posts.partials.create_update_form')
                </form>
            </div>
        </div>
    </div>
@endsection