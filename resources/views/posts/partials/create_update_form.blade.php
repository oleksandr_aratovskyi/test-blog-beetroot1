<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="create-edit-post-title">Title</label>
    <input type="text" name="title" class="form-control" id="create-edit-post-title" placeholder="Title"
           value="{{ isset($post) ? $post->title : old('title')}}">
    @if($errors->has('title'))
        <span class="help-block">{{ $errors->first('title') }}</span>
    @endif
</div>
<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
    <label for="create-post-body">Body</label>
    <textarea name="body" class="form-control" id="create-post-body"
              placeholder="Body">{{ isset($post) ? $post->body : old('body') }}</textarea>
    @if($errors->has('body'))
        <span class="help-block">{{ $errors->first('body') }}</span>
    @endif
</div>
@if(isset($post) and $post->image_link)
    <img class="img-thumbnail" src="{{ $post->image_link }}"/>
@endif
<div class="form-group">
    <label for="create-post-upload-image">Photo</label>
    <input name="photo" type="file" id="create-post-upload-image">
    <p class="help-block ">Upload your photo here</p>
</div>
{{ csrf_field() }}
<input name="user_id" type="hidden" value="{{ Auth::id() }}">
<button type="submit" class="btn btn-default">Submit</button>