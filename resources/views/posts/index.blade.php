@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Your posts</h1>
                <div class="panel-heading-buttons">
                    <a href="{{ route('posts.create') }}">Add new post</a>
                </div>
            </div>

            <div class="panel-body">
                @forelse($posts as $post)
                    <div class="post">
                        <div class="p-title">
                            <h3>{{ $post->title }}</h3>
                            <div class="p-buttons">
                                <a href="{{ route('posts.edit', $post->id) }}">Edit</a>
                                <a href="{{ route('posts.delete', $post->id) }}">Delete</a>
                            </div>
                        </div>
                        <p>Updated at {{ $post->updated_at->diffForHumans() }}</p>
                        <div class="p-body">
                            <div class="pb-image">
                                <img class="img-thumbnail" src="{{ $post->image_link }}" height="150"/>
                            </div>
                            <div class="pb-text">
                                {{ substr($post->body, 0, 600) }}{{ strlen($post->body) > 600 ? '...' : '' }}
                                <br/>
                                <a href="{{ route('posts.show', $post->id) }}">Read more</a>
                            </div>
                        </div>
                    </div>
                @empty
                    No posts yet
                @endforelse
            </div>
            <nav class="pager" aria-label="Page navigation">
                {{ $posts->links() }}
            </nav>
        </div>
    </div>
@endsection