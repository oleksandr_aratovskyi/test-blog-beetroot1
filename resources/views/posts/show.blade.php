@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>{{ $post->title }}</h1>
            </div>
            <div class="panel-body">
                @if($post->image_link)
                    <p>
                        <img class="img-thumbnail" src="{{ $post->image_link }}"/>
                    </p>
                @endif
                <p>{!! nl2br($post->body) !!}</p>
            </div>
        </div>
    </div>
@endsection